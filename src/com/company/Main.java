package com.company;

import java.util.Optional;

public class Main {

    public static void main(String[] args) {
        Stack stack = new Stack();
        stack.push("1");
        stack.push("2");
        stack.push("3");
        stack.push("4");

        System.out.println(stack.get());
        stack.pop();
        System.out.println(stack.get());
        stack.pop();
        System.out.println(stack.get());
        stack.pop();
        stack.pop();
        System.out.println(stack.get());

        Stack stack1 = new Stack();
        stack1.push("a");
        stack1.push("b");
        stack1.push("c");

        System.out.println(stack1.get());
        stack1.pop();
        System.out.println(stack1.get());
        stack1.pop();
        System.out.println(stack1.get());
        stack1.pop();
        stack1.pop();
        System.out.println(stack.get());
    }
}