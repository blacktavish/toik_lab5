package com.company;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class Stack implements StackOperations{

    List<String> items = new ArrayList<>();

    @Override
    public List<String> get() {
        List<String> list = new ArrayList<>();
            for(int i = 1; i <= items.size(); i++){
                list.add(items.get(items.size()-i));
             }
             return list;
    }
    @Override
    public Optional<String> pop() {
        if (items.isEmpty()) {
            return Optional.empty();
        }
        else {
            String top = items.get(items.size() - 1);
            items.remove(items.size() - 1);
            return Optional.ofNullable(top);
        }
    }
    @Override
    public void push(String item) {
            items.add(item);
            //System.out.println(item);
    }
}
